program labPascal;

uses crt,sysutils;

var
	secE,secS:file of char;
	v:char;
	esSalida:boolean;
	km,conBoca,conMesImp,conViajes:integer;
	porMesImp:real;

type secuencia=file of char;

procedure saltoPatente(var sec:secuencia;var v:char);
var
	i:integer;
begin
	for i:=1 to 6 do
	begin
		read(sec,v);
	end;
end;

procedure saltoLicencia(var sec:secuencia;var v:char);
var
	i:integer;
begin
	for i:=1 to 8 do
	begin
		read(sec,v);
	end;
end;

function tomarMes (var sec:secuencia;var v:char):integer;
var
	i:integer;
begin
	tomarMes:=0;
	for i:=1 to 3 do
	begin
		read(sec,v);
	end;
	for i:=1 to 2 do
	begin
		tomarMes:=tomarMes*10+strToInt(v);
		read(sec,v);
	end;
	for i:=1 to 5 do
	begin
		read(sec,v);
	end;
end;

procedure evaMesImp (num:integer);
begin
	if (num mod 2 <> 0)  then
	begin
		conMesImp:=conMesImp+1;
	end;
end;

function kilometraje (var sec:secuencia; var v:char):integer;
var
	i:integer;
begin
	kilometraje:=0;
	for i:=1 to 4 do
	begin
		kilometraje:=kilometraje*10+strToInt(v);
		read(sec,v);
	end;
	km:=kilometraje;
end;

function enKm (num:integer):boolean;
begin
	if (num<=40) and (num>=20) then
	begin
		enKm:=true;
	end
	else
	begin
		enKm:=false;
	end;
end;

function intToChar (num:integer):char;
begin
	case (num) of
		1:
		begin
			intToChar:='1';
		end;
		2:
		begin
			intToChar:='2';
		end;
		3:
		begin
			intToChar:='3';
		end;
		4:
		begin
			intToChar:='4';
		end;
		5:
		begin
			intToChar:='5';
		end;
		6:
		begin
			intToChar:='6';
		end;
		7:
		begin
			intToChar:='7';
		end;
		8:
		begin
			intToChar:='8';
		end;
		9:
		begin
			intToChar:='9';
		end;
		0:
		begin
			intToChar:='0';
		end;
	end;
end;

procedure boca(var sec:secuencia; var v:char; esSalida:boolean);
var
	i:integer; esBoca:boolean;
const
	boca='boca';
begin
	esBoca:=true;
	if (esSalida) then
	begin
		i:=1;
		repeat
			if (lowerCase(v)<>boca[i]) then
			begin
				esBoca:=false;
			end;
			write(secS,v);
			read(sec,v);
			i:=i+1;
		until (v='-');
		if (esBoca) then
		begin
			conBoca:=conBoca+1;
			write(secS,'+');
		end
		else
		begin
			write(secS,'+');
		end;
	end
	else
	begin
		i:=1;
		repeat
			if (v<>boca[i]) or (i>4) then
			begin
				esBoca:=false;
			end;
			read(sec,v);
			i:=i+1;
		until (v='-');
		if (esBoca) then
		begin
			conBoca:=conBoca+1;
		end;
	end;
end;

procedure grabarSalida(var sec:secuencia; var v:char; num:integer);
var
	conG:integer;
begin
	conG:=0;
	write(secS,intToChar(num div 10));
	write(secS,intToChar(num mod 10));
	repeat
		write(secS,v);
		if (v='-') then
		begin
			conG:=conG+1;
		end;
		read(sec,v);
	until (conG=2);
	boca(secE,v,esSalida);
end;

procedure saltearEntrada (var sec:secuencia; var v:char);
var
	conG:integer;
begin
	conG:=0;
	repeat
		if (v='-') then
		begin
			conG:=conG+1;
		end;
		read(sec,v);
	until (conG=2);
	boca(secE,v,esSalida);
end;

procedure saltearDocu(var sec:secuencia; var v:char);
begin
	repeat
		read(sec,v);
	until (v='+');
end;

procedure informe;
begin
	writeln('-----------------------------------');
	case (conMesImp) of
		1:
		begin
			writeln('Hubo 1 viaje realizado en un mes impar');
		end;
		else
			writeln('Se realizaron ', conMesImp, ' viajes en meses impares.');
	end;
	writeln('-----------------------------------');
	writeln('El ', porMesImp:3:2,'% de los viajes se realizan en meses impares.');
	writeln('-----------------------------------');
	case (conBoca) of
		1:
		begin
			writeln('Se realizo 1 viaje con destino a Boca.');
		end;
		else
			writeln('Se realizaron ', conBoca, ' viajes con destino a Boca.');
	end;
	writeln('-----------------------------------');
end;

begin
	conBoca:=0;conMesImp:=0;conViajes:=0;
	assign(secE,'archE.txt');
	assign(secS,'archS.txt');
	reset(secE);
	rewrite(secS);
	read(secE,v);
	while (v<>'*') do
	begin
		while (v<>'+') do
		begin
			saltoPatente(secE,v);
			saltoLicencia(secE,v);
			read(secE,v);// saltear el nivel del conductor
			evaMesImp(tomarMes(secE,v));
			if (enKm(kilometraje(secE,v))) then
			begin
				esSalida:=true;
				grabarSalida(secE,v,km);
			end
			else
			begin
				esSalida:=false;
				saltearEntrada(secE,v);
			end;
			saltearDocu(secE,v);
		end;
		conViajes:=conViajes+1;
		read(secE,v);
	end;
	porMesImp:=conMesImp / conViajes*100;
	informe();
	close(secS);
	close(secE);
end.
